-- phpMyAdmin SQL Dump
-- version 3.3.7
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2017 年 04 月 27 日 00:19
-- 服务器版本: 5.5.49
-- PHP 版本: 5.6.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- 数据库: `jadmin`
--

-- --------------------------------------------------------

--
-- 表的结构 `sequence`
--

CREATE TABLE IF NOT EXISTS `sequence` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `stub` char(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stub` (`stub`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1025 ;

--
-- 转存表中的数据 `sequence`
--

INSERT INTO `sequence` (`id`, `stub`) VALUES
(1024, 'a');

-- --------------------------------------------------------

--
-- 表的结构 `sys_log`
--

CREATE TABLE IF NOT EXISTS `sys_log` (
  `id` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT 'id',
  `title` varchar(255) DEFAULT '' COMMENT '日志标题',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `remote_addr` varchar(255) DEFAULT NULL COMMENT '操作IP地址',
  `user_agent` varchar(255) DEFAULT NULL COMMENT '用户代理',
  `request_uri` varchar(255) DEFAULT NULL COMMENT '请求URI',
  `method` varchar(5) DEFAULT NULL COMMENT '操作方式',
  `params` text COMMENT '操作提交的数据',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记（1：Y，0：N）',
  PRIMARY KEY (`id`),
  KEY `sys_log_create_by` (`create_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `sys_log`
--

INSERT INTO `sys_log` (`id`, `title`, `create_by`, `create_date`, `remote_addr`, `user_agent`, `request_uri`, `method`, `params`, `update_date`, `del_flag`) VALUES
(1000, '用户登录：sysadmin', '1', '2017-04-25 20:27:42', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-25 20:27:42', 0),
(1001, '用户登录：sysadmin', '1', '2017-04-25 20:30:38', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:30:38', 0),
(1002, '用户登录：sysadmin', '1', '2017-04-25 20:39:04', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:39:04', 0),
(1003, '用户登录：sysadmin', '1', '2017-04-25 20:42:05', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:42:05', 0),
(1, '用户登录：sysadmin', '1', '2017-04-25 20:27:42', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-25 20:27:42', 0),
(2, '用户登录：sysadmin', '1', '2017-04-25 20:30:38', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:30:38', 0),
(3, '用户登录：sysadmin', '1', '2017-04-25 20:39:04', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:39:04', 0),
(4, '用户登录：sysadmin', '1', '2017-04-25 20:42:05', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:42:05', 0),
(5, '用户登录：sysadmin', '1', '2017-04-25 20:27:42', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-25 20:27:42', 0),
(6, '用户登录：sysadmin', '1', '2017-04-25 20:30:38', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:30:38', 0),
(7, '用户登录：sysadmin', '1', '2017-04-25 20:39:04', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:39:04', 0),
(8, '用户登录：sysadmin', '1', '2017-04-25 20:27:42', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-25 20:27:42', 0),
(9, '用户登录：sysadmin', '1', '2017-04-25 20:30:38', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:30:38', 0),
(10, '用户登录：sysadmin', '1', '2017-04-25 20:39:04', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:39:04', 0),
(11, '用户登录：sysadmin', '1', '2017-04-25 20:27:42', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-25 20:27:42', 0),
(12, '用户登录：sysadmin', '1', '2017-04-25 20:30:38', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:30:38', 0),
(13, '用户登录：sysadmin', '1', '2017-04-25 20:39:04', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:39:04', 0),
(14, '用户登录：sysadmin', '1', '2017-04-25 20:42:05', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:42:05', 0),
(15, '用户登录：sysadmin', '1', '2017-04-25 20:27:42', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-25 20:27:42', 0),
(16, '用户登录：sysadmin', '1', '2017-04-25 20:30:38', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:30:38', 0),
(17, '用户登录：sysadmin', '1', '2017-04-25 20:39:04', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:39:04', 0),
(18, '用户登录：sysadmin', '1', '2017-04-25 20:27:42', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-25 20:27:42', 0),
(19, '用户登录：sysadmin', '1', '2017-04-25 20:30:38', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:30:38', 0),
(20, '用户登录：sysadmin', '1', '2017-04-25 20:39:04', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:39:04', 0),
(1004, '用户登录：sysadmin', '1', '2017-04-25 20:55:28', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 20:55:28', 0),
(1005, '用户登录：sysadmin', '1', '2017-04-25 21:00:58', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 21:00:58', 0),
(1006, '用户登录：sysadmin', '1', '2017-04-25 21:04:46', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 21:04:46', 0),
(1007, '用户登录：sysadmin', '1', '2017-04-25 21:05:45', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 21:05:45', 0),
(1008, '用户登录：sysadmin', '1', '2017-04-25 21:07:53', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 21:07:53', 0),
(1009, '用户登录：sysadmin', '1', '2017-04-25 21:20:29', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/a', 'GET', '', '2017-04-25 21:20:29', 0),
(1010, '用户登录：sysadmin', '1', '2017-04-25 21:23:40', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-25 21:23:40', 0),
(1013, '用户登录：jsyso', '1012', '2017-04-25 21:26:18', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-25 21:26:18', 0),
(1014, '用户登录：sysadmin', '1', '2017-04-25 21:26:35', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-25 21:26:35', 0),
(1015, '用户登录：jsyso', '1012', '2017-04-25 21:27:03', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/jadmin/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-25 21:27:03', 0),
(1016, '用户登录：jsyso', '1012', '2017-04-25 23:43:11', '113.89.99.157', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36', '/a', 'GET', '', '2017-04-25 23:43:11', 0),
(1017, '用户登录：jsyso', '1012', '2017-04-26 00:10:02', '113.89.99.157', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-26 00:10:02', 0),
(1018, '用户登录：jsyso', '1012', '2017-04-26 14:01:13', '113.89.99.157', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/a', 'GET', '', '2017-04-26 14:01:13', 0),
(1019, '用户登录：jsyso', '1012', '2017-04-26 14:01:20', '101.226.33.206', 'Mozilla/5.0 (Linux; Android 4.4.4; HUAWEI ALE-CL00 Build/HuaweiALE-CL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043024 Safari/537.36 MicroMessenger/6.5.4.1000 NetType/4G Language/zh_CN', '/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-26 14:01:20', 0),
(1020, '用户登录：jsyso', '1012', '2017-04-26 14:02:13', '218.18.112.69', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0', '/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-26 14:02:13', 0),
(1021, '用户登录：jsyso', '1012', '2017-04-26 14:03:20', '113.89.99.157', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-26 14:03:20', 0),
(1022, '用户登录：jsyso', '1012', '2017-04-26 15:47:42', '101.226.66.187', 'Mozilla/5.0 (Linux; Android 4.4.4; HUAWEI ALE-CL00 Build/HuaweiALE-CL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043024 Safari/537.36 MicroMessenger/6.5.4.1000 NetType/4G Language/zh_CN', '/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-26 15:47:42', 0),
(1023, '用户登录：sysadmin', '1', '2017-04-27 00:18:10', '113.89.99.157', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36', '/a', 'GET', '', '2017-04-27 00:18:10', 0),
(1024, '用户登录：sysadmin', '1', '2017-04-27 00:18:49', '113.89.99.157', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36', '/WEB-INF/views/layui/sys/index.jsp', 'GET', '', '2017-04-27 00:18:49', 0);

-- --------------------------------------------------------

--
-- 表的结构 `sys_menu`
--

CREATE TABLE IF NOT EXISTS `sys_menu` (
  `id` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '菜单id',
  `parent_id` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '父级编号id',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `sort` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `href` varchar(2000) DEFAULT NULL COMMENT '链接',
  `target` varchar(20) DEFAULT NULL COMMENT '目标',
  `icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否在菜单中显示（1：Y，0：N）',
  `permission` varchar(200) DEFAULT NULL COMMENT '权限标识',
  `create_date` datetime NOT NULL COMMENT '注册时间',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记（1：Y，0：N）',
  PRIMARY KEY (`id`),
  KEY `sys_menu_parent_id` (`parent_id`),
  KEY `sys_menu_del_flag` (`del_flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='菜单表';

--
-- 转存表中的数据 `sys_menu`
--

INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `sort`, `href`, `target`, `icon`, `is_show`, `permission`, `create_date`, `update_date`, `del_flag`) VALUES
(1, 0, '用户管理', 300, NULL, NULL, 'fa-group', 1, '', '2017-04-05 00:00:00', '2017-04-05 00:00:00', 0),
(2, 1, '菜单管理', 300, '/sys/menu/', NULL, NULL, 1, '', '2017-04-05 00:00:00', '2017-04-05 00:00:00', 0),
(3, 1, '角色管理', 301, '/sys/role/', NULL, NULL, 1, '', '2017-04-05 00:00:00', '2017-04-05 00:00:00', 0),
(4, 1, '管理员', 302, '/sys/user/', NULL, NULL, 1, '', '2017-04-05 00:00:00', '2017-04-05 00:00:00', 0),
(5, 2, '编辑', 0, NULL, NULL, NULL, 0, 'sys:menu:edit', '2017-04-05 00:00:00', '2017-04-05 00:00:00', 0),
(6, 3, '编辑', 0, NULL, NULL, NULL, 0, 'sys:role:edit', '2017-04-05 00:00:00', '2017-04-05 00:00:00', 0),
(7, 4, '编辑', 0, NULL, NULL, NULL, 0, 'sys:user:edit', '2017-04-05 00:00:00', '2017-04-05 00:00:00', 0),
(8, 2, '查看', 0, NULL, NULL, NULL, 0, 'sys:menu:view', '2017-04-05 00:00:00', '2017-04-05 00:00:00', 0),
(9, 3, '查看', 0, NULL, NULL, NULL, 0, 'sys:role:view', '2017-04-05 00:00:00', '2017-04-05 00:00:00', 0),
(10, 4, '查看', 0, NULL, NULL, NULL, 0, 'sys:user:view', '2017-04-05 00:00:00', '2017-04-05 00:00:00', 0),
(1145, 0, '操作日志', 0, '', NULL, 'fa-file-text-o', 1, '', '2017-04-22 02:22:51', '2017-04-22 02:22:51', 0),
(1146, 1145, '操作日志', 0, '/sys/log?pageNo=1', NULL, '', 1, 'sys:log:view', '2017-04-22 02:23:19', '2017-04-22 02:23:19', 0);

-- --------------------------------------------------------

--
-- 表的结构 `sys_role`
--

CREATE TABLE IF NOT EXISTS `sys_role` (
  `id` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `name` varchar(20) NOT NULL COMMENT '角色名称',
  `enname` varchar(200) NOT NULL DEFAULT '' COMMENT '角色名称（英文）',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态（0：禁用，1：开启，2：不可修改）',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记（1：Y，0：N）',
  PRIMARY KEY (`id`),
  KEY `sys_role_del_flag` (`del_flag`),
  KEY `sys_role_enname` (`enname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='角色表';

--
-- 转存表中的数据 `sys_role`
--

INSERT INTO `sys_role` (`id`, `name`, `enname`, `status`, `remark`, `create_date`, `update_date`, `del_flag`) VALUES
(1, '超级管理员', 'sys_super_admin', 1, '系统超级管理员', '2017-04-16 16:53:28', '2017-04-16 16:53:28', 0),
(1011, '测试', 'demo', 1, 'demo', '2017-04-25 21:24:42', '2017-04-25 21:24:42', 0);

-- --------------------------------------------------------

--
-- 表的结构 `sys_role_menu`
--

CREATE TABLE IF NOT EXISTS `sys_role_menu` (
  `role_id` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `menu_id` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '菜单编号',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='角色菜单对于表';

--
-- 转存表中的数据 `sys_role_menu`
--

INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES
(1011, 1),
(1011, 2),
(1011, 3),
(1011, 4),
(1011, 8),
(1011, 9),
(1011, 10),
(1011, 1145),
(1011, 1146);

-- --------------------------------------------------------

--
-- 表的结构 `sys_user`
--

CREATE TABLE IF NOT EXISTS `sys_user` (
  `id` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `login_name` varchar(60) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(100) NOT NULL DEFAULT '' COMMENT '用户密码',
  `job_no` varchar(100) DEFAULT NULL COMMENT '工号',
  `name` varchar(100) NOT NULL COMMENT '姓名',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(200) DEFAULT NULL COMMENT '电话',
  `phone` varchar(200) DEFAULT NULL COMMENT '手机',
  `avatar` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `login_ip` varchar(100) DEFAULT NULL COMMENT '最后登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `login_flag` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否可登录（1：Y，0：N）',
  `create_date` datetime NOT NULL COMMENT '注册时间',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记（1：Y，0：N）',
  PRIMARY KEY (`id`),
  KEY `sys_user_login_name` (`login_name`),
  KEY `sys_user_del_flag` (`del_flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理员表';

--
-- 转存表中的数据 `sys_user`
--

INSERT INTO `sys_user` (`id`, `login_name`, `password`, `job_no`, `name`, `email`, `mobile`, `phone`, `avatar`, `login_ip`, `login_date`, `login_flag`, `create_date`, `update_date`, `del_flag`) VALUES
(1, 'sysadmin', '69e4688e1c640d576ee1663780f7c787651d8ea63bfe75242185bc34', '001', '超级管理员', '12345678@qq.com', '13334567890', '13334567890', 'http://pic.qiantucdn.com/58pic/14/38/47/15w58PICMZS.jpg!qt226', '113.89.99.157', '2017-04-27 00:18:49', 1, '2017-02-14 05:20:00', '2017-04-27 00:18:49', 0),
(1012, 'jsyso', '575bf91b6875344ce03a041287c45b14376351fd1e5a13b5c75e5827', NULL, 'jsyso', NULL, NULL, NULL, NULL, '101.226.66.187', '2017-04-26 15:47:42', 1, '2017-04-25 21:25:57', '2017-04-26 15:47:42', 0);

-- --------------------------------------------------------

--
-- 表的结构 `sys_user_role`
--

CREATE TABLE IF NOT EXISTS `sys_user_role` (
  `role_id` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `user_id` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户角色对应表';

--
-- 转存表中的数据 `sys_user_role`
--

INSERT INTO `sys_user_role` (`role_id`, `user_id`) VALUES
(1, 1),
(1011, 1012);
