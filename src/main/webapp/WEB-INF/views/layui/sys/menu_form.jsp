<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/layui/_public/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Powered By ${fns:getConfig('text.productName')}</title>
<%@include file="../_public/header.jsp" %>
</head>
<body>
<div class="sub-page">
	<div class="layui-tab layui-tab-brief sub-page-tab" lay-filter="F_sub_tab">
		<ul class="layui-tab-title">
			<li data-url="${__ADMIN__ }/sys/menu/">菜单管理</li>
			<!-- 
			<li ${empty param.id ? 'class="layui-this" ' : '' }data-url="${__ADMIN__ }/sys/menu/form/">添加菜单</li>
			<c:if test="${not empty param.id }">
			<li class="layui-this" data-url="${__ADMIN__ }/sys/menu/form?id=${param.id }">编辑菜单</li>
			</c:if>
			 -->
			 <sys:tabTitle url="sys/menu/form" title="菜单"></sys:tabTitle>
		</ul>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show">
				<form:form class="layui-form" id="inputForm" modelAttribute="menu" action="${__ADMIN__}/sys/menu/do_save" method="post">
					<form:hidden path="id"/>
					<div class="layui-form-item">
						<label class="layui-form-label">上级</label>
						<div class="layui-input-inline">
							<form:select path="parentId">
								<form:option value="0" label="作为一级菜单"/>
								<form:options items="${fns:getTreeChainList() }" itemLabel="alias" itemValue="id" htmlEscape="false" />
							</form:select>
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">名称</label>
						<div class="layui-input-inline">
							<input type="text" name="name" value="${menu.name }" lay-verify="required" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">权限标识</label>
						<div class="layui-input-inline">
							<input type="text" name="permission" value="${menu.permission }" class="layui-input">
						</div>
						<div class="layui-form-mid layui-word-aux">格式为：模块:功能:权限（例如：sys:user:view）</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">链接</label>
						<div class="layui-input-inline">
							<input type="text" name="href" value="${menu.href }" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">图标</label>
						<div class="layui-input-inline">
							<input type="text" name="icon" value="${menu.icon }" class="layui-input">
						</div>
						<div class="layui-form-mid layui-word-aux">font-awesome图标名（例如：fa-user）</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">是否显示</label>
						<div class="layui-input-block">
							<input type="hidden" id="J_hdn_isshow" name="isShow" value="${menu.isShow }" />
							<input type="checkbox"${menu.isShow eq 1 ? ' checked=""' : '' } lay-verify="required" lay-skin="switch" 
							lay-filter="F_switch" data-hdnid="#J_hdn_isshow" lay-text="ON|OFF">
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-input-block">
							<button class="layui-btn" data-listurl="${__ADMIN__ }/sys/menu/" lay-submit="" lay-filter="F_do_ajax_submit">确认提交</button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<script src="${__STATIC__ }/admin/js/admin.js?t=<%=System.currentTimeMillis() %>"></script>
<script type="text/javascript">
layui.use(['element','jacommon'], function(){
	var $ = layui.jquery
	,layer = layui.layer
	,jacommon = layui.jacommon;
	
});
</script>
</body>
</html>