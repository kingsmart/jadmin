<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fns" uri="/WEB-INF/tlds/fns.tld" %>
<%@ taglib prefix="shiro" uri="/WEB-INF/tlds/shiros.tld" %>
<%@ taglib prefix="sys" tagdir="/WEB-INF/tags/layui/sys" %>
<c:set var="__URL__" value="${pageContext.request.contextPath}"/>
<c:set var="__ADMIN__" value="${pageContext.request.contextPath}/a"/>
<c:set var="__STATIC__" value="${pageContext.request.contextPath}/static"/>