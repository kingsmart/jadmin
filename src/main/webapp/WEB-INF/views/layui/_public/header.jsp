<%@ page contentType="text/html;charset=UTF-8" %>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="icon" href="${__URL__ }/favicon.png" />
<link rel="shortcut icon" href="${__URL__ }/favicon.png" />
<link rel="stylesheet" href="${__STATIC__ }/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="${__STATIC__ }/layui/css/layui.css" />
<link rel="stylesheet" href="${__STATIC__ }/admin/css/admin.css?t=<%=System.currentTimeMillis() %>" />
<script src="${__STATIC__ }/layui/layui.js"></script>
<script type="text/javascript">
//全局变量
var GLOBAL = {
    STATIC: "${__STATIC__}/",
    ADMIN: "${__ADMIN__}/",
    URL: "${__URL__}/",
};
layui.config({
  base: GLOBAL.STATIC + 'admin/js/'
  ,version: true
});
</script>