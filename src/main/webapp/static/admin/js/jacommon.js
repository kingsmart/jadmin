;layui.define(['layer'], function (exports) {
	"use strict";
	var $ = layui.jquery
	,jacommon = {};
	
	function _empty() {}
	
	// 需要确认弹出框
	jacommon.confirm = function (text, callback, title) {
		layer.confirm(text, {
            title: title || '操作提醒',
            btnAlign: 'c',
            resize: false,
            icon: 3,
            btn: ['确定', '取消'],
            yes: callback
        });
	}
	
	// 成功弹出提醒
	jacommon.alertSuccess = function (text, title) {
        layer.alert(text, { title: title || '温馨提示', icon: 1, time: 5000, resize: false, zIndex: layer.zIndex, anim: Math.ceil(Math.random() * 6) });
    }
	
    // 错误弹出提示
	jacommon.alertError = function (text, title) {
        layer.alert(text, { title: title || '操作提醒', icon: 2, time: 5000, resize: false, zIndex: layer.zIndex, anim: Math.ceil(Math.random() * 6) });
    }
	
	// 成功提醒
	jacommon.success = function (text, callback) {
		layer.msg(text, { icon: 1, time: 2000 }, callback || _empty); 
	}
	
	// 错误提示
	jacommon.error = function (text, callback) {
		layer.msg(text, { icon: 2, time: 2000 }, callback || _empty); 
	}
    
    // ajax请求
	jacommon.ajax = function(url, type, data, success, fail, complete) {
		if ($.isFunction(data)) complete = fail, fail = success, success = data, data = undefined;
    	$.ajax({
            url: url,
            type: type,
            dataType: 'json',
            data: data,
            success: function (res) {
            	res.errorCode == 0 ? (success && success(res)) : (fail && fail(res));
            },
            error: function(xhr, status, error) {
            	(fail && fail({errorCode:-1, msg:error}, xhr, status, error));
            },
            complete: complete
        });
    }
	
	// ajax get请求
	jacommon.ajaxGet = function(url, data, success, error, complete) {
		jacommon.ajax(url, 'get', data, success, error, complete);
	}
	
	// ajax post请求
	jacommon.ajaxPost = function(url, data, success, error, complete) {
		jacommon.ajax(url, 'post', data, success, error, complete);
	}
	
	exports("jacommon", jacommon);
});