package com.jsyso.jadmin.weixin.handle;

import org.springframework.stereotype.Component;

import com.jsyso.weixin.handler.RequestHandler;
import com.jsyso.weixin.msg.in.InTextMsg;
import com.jsyso.weixin.msg.out.OutMsg;
import com.jsyso.weixin.msg.out.OutTextMsg;

/**
 * 文字回复处理
 * @author janjan, xujian_jason@163.com
 *
 */
@Component
public class InTextMsgHandler implements RequestHandler<InTextMsg> {

	@Override
	public OutMsg onHandle(InTextMsg inMsg) {
		OutTextMsg textMsg = new OutTextMsg(inMsg);
		textMsg.setContent("回复："+inMsg.getContent());
		return textMsg;
	}

}
