package com.jsyso.jadmin.weixin.handle;

import org.springframework.stereotype.Component;

import com.jsyso.jsyso.util.JsMap;
import com.jsyso.weixin.api.API;
import com.jsyso.weixin.api.UserAPI;
import com.jsyso.weixin.handler.RequestHandler;
import com.jsyso.weixin.msg.in.event.InFollowEvent;
import com.jsyso.weixin.msg.out.OutMsg;
import com.jsyso.weixin.msg.out.OutTextMsg;
import com.jsyso.weixin.utils.Success;

/**
 * 关注事件处理
 * @author janjan, xujian_jason@163.com
 *
 */
@Component
public class InFollowEventHandler implements RequestHandler<InFollowEvent> {

	@Override
	public OutMsg onHandle(InFollowEvent inMsg) {
		String appid = inMsg.getAppid();
		String openid = inMsg.getFromUserName();
		final OutTextMsg textMsg = new OutTextMsg(inMsg);
		textMsg.setContent("尊敬的未知用户，欢迎您归来！");
		API.create(UserAPI.class).getUserInfo(appid, openid).get(new Success() {
			@Override
			public void onSuccess(JsMap data, String content) {
				textMsg.setContent("尊敬的"+(data.get("nickname", String.class))+"，欢迎您归来，输入任意关键字查看测试！");
			}
		});
		return textMsg;
	}

}
