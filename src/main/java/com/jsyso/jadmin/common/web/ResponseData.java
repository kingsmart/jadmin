package com.jsyso.jadmin.common.web;

/**
 * 返回数据体
 * @author janjan, xujian_jason@163.com
 *
 */
public final class ResponseData {
	
	private int errorCode;	// 错误码
	private String msg;		// 错误消息
	private Object data;	// 返回数据

	public static ResponseData success(Object data) {
		return create(0, "", data);
	}
	
	public static ResponseData error(String msg) {
		return create(-1, msg, null);
	}
	
	public static ResponseData create(int errorCode, String msg, Object data) {
		ResponseData resData = new ResponseData();
		resData.setErrorCode(errorCode);
		resData.setMsg(msg);
		resData.setData(data);
		return resData;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
}
