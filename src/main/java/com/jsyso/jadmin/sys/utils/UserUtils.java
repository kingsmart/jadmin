package com.jsyso.jadmin.sys.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.google.common.collect.Lists;
import com.jsyso.jadmin.common.utils.ShiroUtils;
import com.jsyso.jadmin.common.utils.TreeUtils;
import com.jsyso.jadmin.sys.security.SysPrincipal;
import com.jsyso.jadmin.sys.service.RoleService;
import com.jsyso.jadmin.sys.service.UserService;
import com.jsyso.jsyso.spring.Springs;
import com.jsyso.jsyso.util.JsMap;

/**
 * 用户相关工具类
 * @author janjan, xujian_jason@163.com
 *
 */
public class UserUtils {

	private static final UserService userService = Springs.getBean(UserService.class);
	private static final RoleService roleService = Springs.getBean(RoleService.class);
	
	public static final String CACHE_AUTH_INFO = "authInfo";
	public static final String CACHE_ROLE_LIST = "roleList";
	public static final String CACHE_MENU_LIST = "menuList";
	
	/**
	 * 获取用户角色信息
	 * @param user
	 * @return
	 */
	public static List<JsMap> getRoleList(JsMap user) {
		if(user == null || user.isEmpty() 
				|| !user.containsKey("id")
				|| !user.containsKey("loginName")) {
			return Lists.newArrayList();
		}
		Long userId = user.get("id", Long.class);
		String loginName = user.get("loginName", String.class);
		// 从缓存获取
		List<JsMap> roleList = ShiroUtils.getCache(CACHE_ROLE_LIST);
		if(roleList != null) {
			return roleList;
		}
		roleList = userService.getRoleList(userId, loginName);
		if(!roleList.isEmpty()) {
			ShiroUtils.putCache(CACHE_ROLE_LIST, roleList);
		}
		return roleList;
	}
	
	/**
	 * 获取当前登录用户角色
	 * @return
	 */
	public static List<JsMap> getRoleList() {
		return getRoleList(getUser());
	}
	
	/**
	 * 获取菜单
	 * @param list
	 * @return
	 */
	public static List<JsMap> getMenuList(JsMap user) {
		if(user == null || user.isEmpty() 
				|| !user.containsKey("id")
				|| !user.containsKey("loginName")) {
			return Lists.newArrayList();
		}
		List<JsMap> menuList = ShiroUtils.getCache(CACHE_MENU_LIST);
		if(menuList != null) {
			return menuList;
		}
		Long userId = user.get("id", Long.class);
		String loginName = user.get("loginName", String.class);
		menuList = userService.getMenuList(userId, loginName);
		if(!menuList.isEmpty()) {
			ShiroUtils.putCache(CACHE_MENU_LIST, menuList);
		}
		return menuList;
	}
	
	/**
	 * 获取当前用户菜单
	 * @return
	 */
	public static List<JsMap> getMenuList() {
		return getMenuList(getUser());
	}
	
	/**
	 * 获取菜单列表，数据做Tree处理
	 * @return
	 */
	public static List<JsMap> getTreeMenuList() {
		List<JsMap> menuList = getMenuList();
		return menuList == null ? new ArrayList<JsMap>()
				: TreeUtils.create(menuList).getTree(2);
	}
	
	/**
	 * 获取当前登录用户
	 * @return
	 */
	public static JsMap getUser() {
		SysPrincipal principal = ShiroUtils.getPrincipal();
		JsMap user = null;
		if(principal != null) {
			user = userService.get(principal.getId());
		}
		if(user == null) {
			user = JsMap.create(0);
		}
		return user;
	}
	
	public static SysPrincipal getPrincipal() {
		SysPrincipal principal = ShiroUtils.getPrincipal();
		return principal;
	}
	
	/**
	 * 清除当前用户缓存
	 */
	public static void clearCache() {
		ShiroUtils.removeCache(CACHE_AUTH_INFO);
		ShiroUtils.removeCache(CACHE_ROLE_LIST);
		ShiroUtils.removeCache(CACHE_MENU_LIST);
		userService.clearCache(getUser());
	}
	
	/**
	 * 当前用户是否是超级管理员
	 */
	public static boolean isSysAdmin() {
		Subject subject = SecurityUtils.getSubject();
		return subject.hasRole(RoleService.SYS_SUPER_ADMIN);
	}
	
	/**
	 * 指定用户是否是超级管理员
	 */
	public static boolean isSysAdmin(Long userId) {
		// 查询用户
		List<JsMap> roles = userService.userRoleByUserId(userId);
		// 超级管理员角色
		JsMap superAdmin = roleService.get(JsMap.create("enname", RoleService.SYS_SUPER_ADMIN));
		for(JsMap role : roles) {
			if(superAdmin != null 
					&& role.get("roleId", 0l, Long.class).longValue() == superAdmin.get("id", -1l, Long.class).longValue())
				return true;
		}
		return false;
	}
}
