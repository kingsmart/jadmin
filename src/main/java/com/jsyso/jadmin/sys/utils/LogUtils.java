package com.jsyso.jadmin.sys.utils;

import javax.servlet.http.HttpServletRequest;

import com.jsyso.jadmin.common.config.Global;
import com.jsyso.jadmin.sys.service.LogService;
import com.jsyso.jsyso.json.jackson.JsonMapper;
import com.jsyso.jsyso.spring.Springs;
import com.jsyso.jsyso.util.JsMap;
import com.jsyso.jsyso.web.WebUtils;

/**
 * 日志工具类
 * @author janjan, xujian_jason@163.com
 *
 */
public abstract class LogUtils {

	private static final LogService logService = Springs.getBean(LogService.class);

	public static void add(final String title, final HttpServletRequest request, final boolean saveParams) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				JsMap user = UserUtils.getUser();
				JsMap data = JsMap.create("del_flag", Global.NO)
				.set("create_by", user.get("id", 0l, Long.class))
				.set("title", title)
				.set("remote_addr", WebUtils.getRemoteAddr(request))
				.set("request_uri", request.getRequestURI())
				.set("method", request.getMethod())
				.set("params", saveParams ? JsonMapper.getInstance().toJson(request.getParameterMap()) : "")
				.set("user_agent", request.getHeader("user-agent"));
				logService.add(data);
			}
		}).start();;
	}
	
	public static void add(final String title, final HttpServletRequest request) {
		add(title, request, true);
	}
}
