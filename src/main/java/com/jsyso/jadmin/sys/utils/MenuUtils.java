package com.jsyso.jadmin.sys.utils;

import java.util.List;

import com.jsyso.jadmin.sys.service.MenuService;
import com.jsyso.jsyso.spring.Springs;
import com.jsyso.jsyso.util.JsMap;

/**
 * 菜单工具类
 * @author janjan, xujian_jason@163.com
 *
 */
public abstract class MenuUtils {

	private static final MenuService menuService = Springs.getBean(MenuService.class);
	
	public static List<Object> getTreeChainList() {
		return menuService.treeChainList();
	}
	
	public static List<JsMap> getChainTree() {
		return menuService.getChainTree();
	}
	
}
