package com.jsyso.jadmin.sys.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.jsyso.jadmin.common.config.Global;
import com.jsyso.jadmin.common.service.BaseService;
import com.jsyso.jadmin.common.utils.TreeUtils;
import com.jsyso.jsyso.db.Dao;
import com.jsyso.jsyso.util.AbsHandler;
import com.jsyso.jsyso.util.Arrays;
import com.jsyso.jsyso.util.JsMap;

@Service
public class MenuService extends BaseService {
	
	@Override
	protected Dao dao() {
		return Dao.get("sys_menu");
	}
	
	public List<Object> treeChainList() {
		List<JsMap> list = this.findList(JsMap.create("del_flag", Global.NO));
		list = TreeUtils.create(list).getChainTree();
		final List<Object> newList = new ArrayList<Object>();
		Arrays.each(list, new AbsHandler<JsMap>() {
			public void doHandle(JsMap param) {
				if(param != null && !param.isEmpty())
					newList.add(param.getBean());
			}
		});
		return newList;
	}
	
	public List<JsMap> getChainTree() {
		List<JsMap> list = this.findList(JsMap.create("del_flag", Global.NO));
		return TreeUtils.create(list).getChainTree();
	}
	
	public List<JsMap> getTree() {
		List<JsMap> list = this.findList(JsMap.create("del_flag", Global.NO));
		return TreeUtils.create(list).getTree();
	}
}
