package com.jsyso.jadmin.sys.service;

import org.springframework.stereotype.Service;

import com.jsyso.jadmin.common.service.BaseService;
import com.jsyso.jsyso.db.Dao;

@Service
public class LogService extends BaseService {

	@Override
	protected Dao dao() {
		return Dao.get("sys_log");
	}
	
}
