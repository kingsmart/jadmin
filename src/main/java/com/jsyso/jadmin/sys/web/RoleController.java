package com.jsyso.jadmin.sys.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jsyso.jadmin.common.config.Global;
import com.jsyso.jadmin.common.web.BaseController;
import com.jsyso.jadmin.sys.service.RoleService;
import com.jsyso.jsyso.util.AbsHandler;
import com.jsyso.jsyso.util.Arrays;
import com.jsyso.jsyso.util.JsMap;
import com.jsyso.jsyso.web.JsMapParam;

/**
 * 角色控制器
 * @author janjan, xujian_jason@163.com
 *
 */
@Controller
@RequestMapping(value = "/a/sys/role")
public class RoleController extends BaseController {
	
	@Autowired
	private RoleService roleService;
	
	@ModelAttribute("role")
	public Object get(@RequestParam(
			required=false, defaultValue="0") Long id) {
		JsMap result = null;
		if(id != null && id > 0) 
			result = roleService.get(id);
		if(result == null)
			result = JsMap.create(8)
			.set("id", 0)
			.set("name", "")
			.set("enname", "")
			.set("status", 1)
			.set("remark", "");
		return result.getBean();
	}
	
	@RequiresPermissions("sys:role:view")
	@RequestMapping(value = {"list", ""})
	public String list(
			HttpServletRequest request, HttpServletResponse response, Model model) {
		List<JsMap> list = roleService.findList(JsMap.create("del_flag", Global.NO));
		model.addAttribute("list", list);
		return this.display("sys/role_list");
	}

	@RequiresPermissions("sys:role:view")
	@RequestMapping(value = "form")
	public String form(
			Long id,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		return this.display("sys/role_form");
	}
	
	@RequiresPermissions("sys:role:view")
	@RequestMapping(value = "assign")
	public String assign(
			Long id,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		// 查询当前角色对应的权限
		List<JsMap> roleMenuList = roleService.roleMenuByRoleId(id);
		final List<Long> menuIdList = new ArrayList<Long>();
		Arrays.each(roleMenuList, new AbsHandler<JsMap>() {
			public void doHandle(JsMap param) {
				menuIdList.add(param.get("menuId", 0l, Long.class));
			}
		});
		model.addAttribute("menuIdString", String.format("[%s]", StringUtils.join(menuIdList, ',')));
		return this.display("sys/role_assign");
	}
	
	@RequiresPermissions("sys:role:edit")
	@RequestMapping(value = "do_save", method = RequestMethod.POST)
	public String doSave(
			JsMapParam role, 
			HttpServletRequest request, HttpServletResponse response, Model model) {
		JsMap data = role.getData();
		Long id = data.get("id", 0l, Long.class);
		String enname = data.get("enname", "", String.class);
		// 检查英文名是否已经存在
		JsMap enRole = roleService.get(JsMap.create("enname", enname));
		if(enRole != null && !enRole.isEmpty() 
				&& !enRole.get("id", 0l, Long.class).equals(id))
			return error(response, "英文名：" + enname + "，已经存在！");
		if(id > 0)
			roleService.save(roleService.keyRename(data));
		else 
			roleService.add(roleService.keyRename(data));
		return success(response, "");
	}
	
	@RequiresPermissions("sys:role:edit")
	@RequestMapping(value = "do_assign", method = RequestMethod.POST)
	public void doAssign(
			Long id,
			String[] ids,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		// 删除旧的权限映射
		roleService.deleteRoleMenu(JsMap.create("role_id", id));
		// 添加新的权限映射
		List<JsMap> datas = new ArrayList<JsMap>();
		for(String menuId : ids) {
			datas.add(JsMap.create("role_id", id).set("menu_id", Long.parseLong(menuId)));
		}
		roleService.addRoleMenuAll(datas);
		success(response, "");
	}

	@RequiresPermissions("sys:role:edit")
	@RequestMapping(value = "do_delete")
	public void doDelete(
			Long id,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		if(id == null || id < 1)
			error(response, "参数错误");
		roleService.delete(JsMap.create("id", id));
		success(response, "");
	}
	
}
