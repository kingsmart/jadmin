package com.jsyso.jadmin.sys.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jsyso.jadmin.common.utils.ShiroUtils;
import com.jsyso.jadmin.common.web.BaseController;

@Controller
public class LoginController extends BaseController {
	
	/**
	 * 管理界面首页
	 */
	@RequiresPermissions("user")
	@RequestMapping(value = "/a")
	public String index(HttpServletRequest request, HttpServletResponse response) {
		return this.display("sys/index");
	}
	
	@RequestMapping(value = "/a/login", method = RequestMethod.GET)
	public String login(HttpServletRequest request, HttpServletResponse response, Model model) {
		if(ShiroUtils.isLogin()) {
			return this.redirect("/a");
		}
		return this.display("sys/login");
	}
	
}
