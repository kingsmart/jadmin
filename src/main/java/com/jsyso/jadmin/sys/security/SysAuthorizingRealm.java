package com.jsyso.jadmin.sys.security;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.jsyso.jadmin.common.config.Global;
import com.jsyso.jadmin.common.security.Password;
import com.jsyso.jadmin.common.utils.Encodes;
import com.jsyso.jadmin.common.utils.ShiroUtils;
import com.jsyso.jadmin.common.web.ValidateCodeServlet;
import com.jsyso.jadmin.sys.service.UserService;
import com.jsyso.jadmin.sys.utils.LogUtils;
import com.jsyso.jadmin.sys.utils.UserUtils;
import com.jsyso.jsyso.spring.Springs;
import com.jsyso.jsyso.util.JsMap;
import com.jsyso.jsyso.web.WebUtils;

/**
 * 安全认证实现类（系统管理模块）
 * @author janjan, xujian_jason@163.com
 *
 */
@Component
public class SysAuthorizingRealm extends AuthorizingRealm {
	
	public SysAuthorizingRealm() {
		this.setCachingEnabled(false);
	}
	
	private UserService userService;
	
	/**
	 * 获取用户Service
	 * @return
	 */
	private UserService getUserService() {
		if(userService == null) {
			userService = Springs.getBean(UserService.class);
		}
		return userService;
	}
	
	/**
	 * 初始化
	 * 设定密码校验的Hash算法与迭代次数
	 */
	@PostConstruct
	public void initCredentialsMatcher() {
		HashedCredentialsMatcher matcher = new HashedCredentialsMatcher(Password.HASH_ALGORITHM);
		matcher.setHashIterations(Password.HASH_INTERATIONS);
		super.setCredentialsMatcher(matcher);
	}
	
	/**
	 * 获取当前请求对象
	 * @return
	 */
	public HttpServletRequest getRequest(){
		try{
			return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		}catch(Exception e){
			return null;
		}
	}
	
	/* 
	 * 为当前登录的Subject授予角色和权限 
	 * @see org.apache.shiro.realm.AuthorizingRealm#doGetAuthorizationInfo(org.apache.shiro.subject.PrincipalCollection)
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		SysPrincipal principal = (SysPrincipal) super.getAvailablePrincipal(principals);
//		JsMap user = getUserService().get(principal.getId());
		if(principal != null) {
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			
			// 获取角色对于权限信息
			List<JsMap> menus = UserUtils.getMenuList();
			for(JsMap menu: menus) {
				String permissionStr = menu.get("permission", String.class);
				if(StringUtils.isNotBlank(permissionStr)) {
					for (String permission : StringUtils.split(permissionStr,",")){
						info.addStringPermission(permission);
					}
				}
			}
			
			// 获取当前用户角色信息
			List<JsMap> roles = UserUtils.getRoleList();
			// 添加用户权限
			info.addStringPermission("user");
			for(JsMap role: roles) {
				info.addRole(role.get("enname", String.class));
			}
						
			// 更新用户登录信息
			getUserService().save(JsMap.create("login_ip", WebUtils.getRemoteAddr(getRequest()))
					.set("login_date", new Date()).set("id", principal.getId()));
			
			// 记录登录日志
			LogUtils.add("用户登录：" + principal.getLoginName(), getRequest(), false);
			return info;
		} else {
			return null;
		}
	}
	
	/**
	 * 获取权限授权信息，如果缓存中存在，则直接从缓存中获取，否则就重新获取， 登录成功后调用
	 */
	@Override
	protected AuthorizationInfo getAuthorizationInfo(PrincipalCollection principals) {
		// return super.getAuthorizationInfo(principals);
		if (principals == null) {
            return null;
        }
		AuthorizationInfo info = ShiroUtils.getCache(UserUtils.CACHE_AUTH_INFO);
		if(info == null) {
			info = doGetAuthorizationInfo(principals);
			if(info != null) {
				ShiroUtils.putCache(UserUtils.CACHE_AUTH_INFO, info);
			}
		}
		return info;
	}

	/* 
	 * 验证当前登录Subject
	 * @see org.apache.shiro.realm.AuthenticatingRealm#doGetAuthenticationInfo(org.apache.shiro.authc.AuthenticationToken)
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		// Token
		SysUsernamePasswordToken sysToken = (SysUsernamePasswordToken)token;
		// Session
		Session session = ShiroUtils.getSession();
		// 验证码
		if(session != null) {
			String vcode = (String)session.getAttribute(ValidateCodeServlet.VALIDATE_CODE);
			if(StringUtils.isBlank(vcode) 
					|| sysToken == null 
					|| StringUtils.isBlank(sysToken.getCaptcha())
					|| !StringUtils.equalsIgnoreCase(sysToken.getCaptcha(), vcode)) {
				throw new AuthenticationException("msg:验证码错误，请重试！");
			}
		}
		// 用户名密码验证
		JsMap user = getUserService().getByLoginName(sysToken.getUsername());
		if(user != null && !user.isEmpty()) {
			if (Global.NO == user.get("loginFlag", 0, Integer.class)){
				throw new AuthenticationException("msg:该已帐号禁止登录！");
			}
			byte[] salt = Encodes.decodeHex(user.get("password", String.class).substring(0,16));
			return new SimpleAuthenticationInfo(new SysPrincipal(user), 
					user.get("password", String.class).substring(16), 
					ByteSource.Util.bytes(salt), 
					getName());
		} else {
			return null;
		}
	}
	
}
