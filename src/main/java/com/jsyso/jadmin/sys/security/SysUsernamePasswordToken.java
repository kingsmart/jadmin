package com.jsyso.jadmin.sys.security;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * 令牌类
 * username：用户名
 * password：密码
 * captcha：验证码
 * @author janjan, xujian_jason@163.com
 *
 */
public class SysUsernamePasswordToken extends UsernamePasswordToken {

	private static final long serialVersionUID = 1L;

	private String captcha;		// 验证码

	public SysUsernamePasswordToken() {
		super();
	}
	
	public SysUsernamePasswordToken(
			String username, 	// 用户名
			char[] password,	// 密码
			boolean rememberMe, // 是否记住我
			String host, 		// IP地址
			String captcha		// 验证码
			) {
		super(username, password, rememberMe, host);
		this.captcha = captcha;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
	
}
