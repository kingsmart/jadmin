package com.jsyso.jadmin.sys.security;

import java.io.Serializable;

import com.jsyso.jsyso.util.JsMap;

public class SysPrincipal implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id; 			// 编号
	private String loginName; 	// 登录名
	private String name; 		// 姓名

	public SysPrincipal() {
		this(null);
	}
	
	public SysPrincipal(JsMap user) {
		super();
		if(user != null) {
			this.id = user.get("id", Long.class);
			this.loginName = user.get("loginName", String.class);
			this.name = user.get("name", String.class);
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
